import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import * as $ from "jquery";
import { StyleguideService } from "../services/styleguide.service";
import { ObjectsService } from "../services/objects.service";

@Component({
  selector: "app-button",
  templateUrl: "./button.component.html",
  styleUrls: ["./button.component.scss"]
})
export class ButtonComponent implements OnInit {
  showCustomButtons = [];

  constructor(public sg: StyleguideService, public router: Router) {
    this.applyChanges();
    // this.setHover();
    // this.initShowCustomButtons();
  }

  ngOnInit() {}

  applyChanges() {
    $(document).ready(() => {
      setTimeout(() => {
        // size
        // $("button.change").css("font-size", this.sg.button.button.size + "px");
        // // weight
        // $("button.change").css(
        //   "font-weight",
        //   this.sg.button.button.weight + ""
        // );
        // // family
        // $("button.change").css(
        //   "font-family",
        //   this.sg.button.button.family + ""
        // );
        // // width
        // $("button.change").css("width", this.sg.button.button.width + "px");
        // // height
        // $("button.change").css("height", this.sg.button.button.height + "px");
        // // color
        // $("button.change").css("color", this.sg.button.button.color);
        // // background-color
        // $("button.change").css(
        //   "background-color",
        //   this.sg.button.button.background + ""
        // );
        // // radius
        // $("button.change").css(
        //   "border-radius",
        //   this.sg.button.button.radius + "%"
        // );
        // // margin
        // $("button.change").css(
        //   "margin-top",
        //   this.sg.button.button.marginTop + "px"
        // );
        // $("button.change").css(
        //   "margin-right",
        //   this.sg.button.button.marginRight + "px"
        // );
        // $("button.change").css(
        //   "margin-bottom",
        //   this.sg.button.button.marginBottom + "px"
        // );
        // $("button.change").css(
        //   "margin-left",
        //   this.sg.button.button.marginLeft + "px"
        // );
        // // padding
        // $("button.change").css(
        //   "padding-top",
        //   this.sg.button.button.paddingTop + "px"
        // );
        // $("button.change").css(
        //   "padding-right",
        //   this.sg.button.button.paddingRight + "px"
        // );
        // $("button.change").css(
        //   "padding-bottom",
        //   this.sg.button.button.paddingBottom + "px"
        // );
        // $("button.change").css(
        //   "padding-left",
        //   this.sg.button.button.paddingLeft + "px"
        // );
        // // border-color
        // $("button.change").css("border-color", this.sg.button.button.border);
      }, 100);
    });
  }

  //   setHover() {
  //     // hover
  //     const hover = this.sg.button.button.hover;
  //     const bg = this.sg.button.button.background;

  //     console.warn(hover);

  //     $(document).ready(() => {
  //       $(".change").hover(
  //         function() {
  //           $(this).css("background-color", hover);
  //         },
  //         function() {
  //           $(this).css("background-color", bg);
  //         }
  //       );
  //     });
  //   }

  //   next() {
  //     this.router.navigateByUrl("/pdf");
  //   }

  //   back() {
  //     this.router.navigateByUrl("/font");
  //   }

  //   initShowCustomButtons() {
  //     for (let i = 0; i < 6; i++) {
  //       this.showCustomButtons.push(false);
  //     }
  //   }

  //   toggleShowButton(i: number) {
  //     this.showCustomButtons.fill(false);
  //     this.showCustomButtons[i] = true;
  //   }
}
