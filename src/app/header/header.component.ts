import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { StyleguideService } from '../services/styleguide.service';
import { ObjectsService } from '../services/objects.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    start = this.objects.getStart();
    colors = this.objects.getColor();
    font = this.objects.getFont();
    button = this.objects.getButton();

    constructor(public sg: StyleguideService, public router: Router, public objects: ObjectsService) {
    }
    ngOnInit() {
    }

    // next() {

    //     if (this.router.url == '/start') {
    //         this.sg.setStart(this.start);
    //         this.router.navigateByUrl('/color');
    //     }

    //     else if (this.router.url == '/color') {
    //         this.sg.setColor(this.colors);
    //         this.router.navigateByUrl('/font');
    //     }

    //     else if (this.router.url == '/font') {
    //         this.sg.setFont(this.font);
    //         this.router.navigateByUrl('/button');
    //     }

    //     else if (this.router.url == '/button') {
    //         this.sg.setButton(this.button);
    //         this.router.navigateByUrl('/pdf');
    //     }
    // }

    // back() {

    //     if (this.router.url == '/color') {
    //         this.router.navigateByUrl('/start');
    //     }

    //     if (this.router.url == '/font') {
    //         this.router.navigateByUrl('/color');
    //     }

    //     if (this.router.url == '/button') {
    //         this.router.navigateByUrl('/font');
    //     }

    //     if (this.router.url == '/pdf') {
    //         this.router.navigateByUrl('/button');
    //     }
    // }
}
