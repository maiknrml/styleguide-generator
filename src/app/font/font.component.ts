import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import * as $ from "jquery";
import * as WebFont from "webfontloader";
import { StyleguideService } from "../services/styleguide.service";
import { ObjectsService } from "../services/objects.service";

@Component({
  selector: "app-font",
  templateUrl: "./font.component.html",
  styleUrls: ["./font.component.scss"]
})
export class FontComponent implements OnInit {
  font = this.objects.getFont();

  constructor(
    public sg: StyleguideService,
    public router: Router,
    public objects: ObjectsService
  ) {
    this.applyChanges();
  }

  ngOnInit() {}

  applyChanges() {
    $(document).ready(() => {
      this.changeFontFamily();

      console.log(this.font.custom1);

      setTimeout(() => {
        // size
        $("h1.change").css("font-size", this.font.h1.size + "px");
        $("h2.change").css("font-size", this.font.h2.size + "px");
        $("h3.change").css("font-size", this.font.h3.size + "px");
        $("h4.change").css("font-size", this.font.h4.size + "px");
        $("h5.change").css("font-size", this.font.h5.size + "px");
        $("h6.change").css("font-size", this.font.h6.size + "px");
        $("p.change").css("font-size", this.font.p.size + "px");
        $("a.change").css("font-size", this.font.a.size + "px");
        $("p." + this.font.custom1.class).css(
          "font-size",
          this.font.custom1.size + "px"
        );
        $("p." + this.font.custom2.class).css(
          "font-size",
          this.font.custom2.size + "px"
        );
        $("p." + this.font.custom3.class).css(
          "font-size",
          this.font.custom3.size + "px"
        );
        $("p." + this.font.custom4.class).css(
          "font-size",
          this.font.custom4.size + "px"
        );
        $("p." + this.font.custom5.class).css(
          "font-size",
          this.font.custom5.size + "px"
        );
        $("p." + this.font.custom6.class).css(
          "font-size",
          this.font.custom6.size + "px"
        );

        // weight
        $("h1.change").css("font-weight", this.font.h1.weight + "");
        $("h2.change").css("font-weight", this.font.h2.weight + "");
        $("h3.change").css("font-weight", this.font.h3.weight + "");
        $("h4.change").css("font-weight", this.font.h4.weight + "");
        $("h5.change").css("font-weight", this.font.h5.weight + "");
        $("h6.change").css("font-weight", this.font.h6.weight + "");
        $("p.change").css("font-weight", this.font.p.weight + "");
        $("a.change").css("font-weight", this.font.a.weight + "");
        $("p." + this.font.custom1.class).css(
          "font-weight",
          this.font.custom1.weight + "px"
        );
        $("p." + this.font.custom2.class).css(
          "font-weight",
          this.font.custom2.weight + "px"
        );
        $("p." + this.font.custom3.class).css(
          "font-weight",
          this.font.custom3.weight + "px"
        );
        $("p." + this.font.custom4.class).css(
          "font-weight",
          this.font.custom4.weight + "px"
        );
        $("p." + this.font.custom5.class).css(
          "font-weight",
          this.font.custom5.weight + "px"
        );
        $("p." + this.font.custom6.class).css(
          "font-weight",
          this.font.custom6.weight + "px"
        );

        // bold
        $("h1.change").css(
          "font-weight",
          this.font.h1.bold ? "bold" : "initial"
        );
        $("h2.change").css(
          "font-weight",
          this.font.h2.bold ? "bold" : "initial"
        );
        $("h3.change").css(
          "font-weight",
          this.font.h3.bold ? "bold" : "initial"
        );
        $("h4.change").css(
          "font-weight",
          this.font.h4.bold ? "bold" : "initial"
        );
        $("h5.change").css(
          "font-weight",
          this.font.h5.bold ? "bold" : "initial"
        );
        $("h6.change").css(
          "font-weight",
          this.font.h6.bold ? "bold" : "initial"
        );
        $("p.change").css("font-weight", this.font.p.bold ? "bold" : "initial");
        $("a.change").css("font-weight", this.font.a.bold ? "bold" : "initial");
        $("p." + this.font.custom1.class).css(
          "font-weight",
          this.font.custom1.bold ? "bold" : "initial"
        );
        $("p." + this.font.custom2.class).css(
          "font-weight",
          this.font.custom2.bold ? "bold" : "initial"
        );
        $("p." + this.font.custom3.class).css(
          "font-weight",
          this.font.custom3.bold ? "bold" : "initial"
        );
        $("p." + this.font.custom4.class).css(
          "font-weight",
          this.font.custom4.bold ? "bold" : "initial"
        );
        $("p." + this.font.custom5.class).css(
          "font-weight",
          this.font.custom5.bold ? "bold" : "initial"
        );
        $("p." + this.font.custom6.class).css(
          "font-weight",
          this.font.custom6.bold ? "bold" : "initial"
        );

        // underline
        $("h1.change").css(
          "text-decoration",
          this.font.h1.underline ? "underline" : "none"
        );
        $("h2.change").css(
          "text-decoration",
          this.font.h2.underline ? "underline" : "none"
        );
        $("h3.change").css(
          "text-decoration",
          this.font.h3.underline ? "underline" : "none"
        );
        $("h4.change").css(
          "text-decoration",
          this.font.h4.underline ? "underline" : "none"
        );
        $("h5.change").css(
          "text-decoration",
          this.font.h5.underline ? "underline" : "none"
        );
        $("h6.change").css(
          "text-decoration",
          this.font.h6.underline ? "underline" : "none"
        );
        $("p.change").css(
          "text-decoration",
          this.font.p.underline ? "underline" : "none"
        );
        $("a.change").css(
          "text-decoration",
          this.font.a.underline ? "underline" : "none"
        );
        $("p." + this.font.custom1.class).css(
          "text-decoration",
          this.font.custom1.underline ? "underline" : "none"
        );
        $("p." + this.font.custom2.class).css(
          "text-decoration",
          this.font.custom2.underline ? "underline" : "none"
        );
        $("p." + this.font.custom3.class).css(
          "text-decoration",
          this.font.custom3.underline ? "underline" : "none"
        );
        $("p." + this.font.custom4.class).css(
          "text-decoration",
          this.font.custom4.underline ? "underline" : "none"
        );
        $("p." + this.font.custom5.class).css(
          "text-decoration",
          this.font.custom5.underline ? "underline" : "none"
        );
        $("p." + this.font.custom6.class).css(
          "text-decoration",
          this.font.custom6.underline ? "underline" : "none"
        );

        // italic
        $("h1.change").css(
          "font-style",
          this.font.h1.italic ? "italic" : "normal"
        );
        $("h2.change").css(
          "font-style",
          this.font.h2.italic ? "italic" : "normal"
        );
        $("h3.change").css(
          "font-style",
          this.font.h3.italic ? "italic" : "normal"
        );
        $("h4.change").css(
          "font-style",
          this.font.h4.italic ? "italic" : "normal"
        );
        $("h5.change").css(
          "font-style",
          this.font.h5.italic ? "italic" : "normal"
        );
        $("h6.change").css(
          "font-style",
          this.font.h6.italic ? "italic" : "normal"
        );
        $("p.change").css(
          "font-style",
          this.font.p.italic ? "italic" : "normal"
        );
        $("a.change").css(
          "font-style",
          this.font.a.italic ? "italic" : "normal"
        );
        $("p." + this.font.custom1.class).css(
          "font-style",
          this.font.custom1.italic ? "italic" : "normal"
        );
        $("p." + this.font.custom2.class).css(
          "font-style",
          this.font.custom2.italic ? "italic" : "normal"
        );
        $("p." + this.font.custom3.class).css(
          "font-style",
          this.font.custom3.italic ? "italic" : "normal"
        );
        $("p." + this.font.custom4.class).css(
          "font-style",
          this.font.custom4.italic ? "italic" : "normal"
        );
        $("p." + this.font.custom5.class).css(
          "font-style",
          this.font.custom5.italic ? "italic" : "normal"
        );
        $("p." + this.font.custom6.class).css(
          "font-style",
          this.font.custom6.italic ? "italic" : "normal"
        );

        // letter spacing
        $("h1.change").css("letter-spacing", this.font.h1.spacing + "px");
        $("h2.change").css("letter-spacing", this.font.h2.spacing + "px");
        $("h3.change").css("letter-spacing", this.font.h3.spacing + "px");
        $("h4.change").css("letter-spacing", this.font.h4.spacing + "px");
        $("h5.change").css("letter-spacing", this.font.h5.spacing + "px");
        $("h6.change").css("letter-spacing", this.font.h6.spacing + "px");
        $("p.change").css("letter-spacing", this.font.p.spacing + "px");
        $("a.change").css("letter-spacing", this.font.a.spacing + "px");
        $("p." + this.font.custom1.class).css(
          "letter-spacing",
          this.font.custom1.spacing + "px"
        );
        $("p." + this.font.custom2.class).css(
          "letter-spacing",
          this.font.custom2.spacing + "px"
        );
        $("p." + this.font.custom3.class).css(
          "letter-spacing",
          this.font.custom3.spacing + "px"
        );
        $("p." + this.font.custom4.class).css(
          "letter-spacing",
          this.font.custom4.spacing + "px"
        );
        $("p." + this.font.custom5.class).css(
          "letter-spacing",
          this.font.custom5.spacing + "px"
        );
        $("p." + this.font.custom6.class).css(
          "letter-spacing",
          this.font.custom6.spacing + "px"
        );

        // family
        $("h1.change").css("font-family", this.font.h1.family + "");
        $("h2.change").css("font-family", this.font.h2.family + "");
        $("h3.change").css("font-family", this.font.h3.family + "");
        $("h4.change").css("font-family", this.font.h4.family + "");
        $("h5.change").css("font-family", this.font.h5.family + "");
        $("h6.change").css("font-family", this.font.h6.family + "");
        $("p.change").css("font-family", this.font.p.family + "");
        $("a.change").css("font-family", this.font.a.family + "");
        $("p." + this.font.custom1.class).css(
          "font-family",
          this.font.custom1.family + ""
        );
        $("p." + this.font.custom2.class).css(
          "font-family",
          this.font.custom2.family + ""
        );
        $("p." + this.font.custom3.class).css(
          "font-family",
          this.font.custom3.family + ""
        );
        $("p." + this.font.custom4.class).css(
          "font-family",
          this.font.custom4.family + ""
        );
        $("p." + this.font.custom5.class).css(
          "font-family",
          this.font.custom5.family + ""
        );
        $("p." + this.font.custom6.class).css(
          "font-family",
          this.font.custom6.family + ""
        );

        // color
        $("h1.change").css("color", this.font.h1.color + "");
        $("h2.change").css("color", this.font.h2.color + "");
        $("h3.change").css("color", this.font.h3.color + "");
        $("h4.change").css("color", this.font.h4.color + "");
        $("h5.change").css("color", this.font.h5.color + "");
        $("h6.change").css("color", this.font.h6.color + "");
        $("p.change").css("color", this.font.p.color + "");
        $("a.change").css("color", this.font.a.color + "");
        $("p." + this.font.custom1.class).css(
          "color",
          this.font.custom1.color + ""
        );
        $("p." + this.font.custom2.class).css(
          "color",
          this.font.custom2.color + ""
        );
        $("p." + this.font.custom3.class).css(
          "color",
          this.font.custom3.color + ""
        );
        $("p." + this.font.custom4.class).css(
          "color",
          this.font.custom4.color + ""
        );
        $("p." + this.font.custom5.class).css(
          "color",
          this.font.custom5.color + ""
        );
        $("p." + this.font.custom6.class).css(
          "color",
          this.font.custom6.color + ""
        );
      }, 100);
    });
  }

  changeFontFamily() {
    WebFont.load({
      google: {
        families: [
          this.font.h1.family,
          this.font.h2.family,
          this.font.h3.family,
          this.font.h4.family,
          this.font.h5.family,
          this.font.h6.family,
          this.font.p.family,
          this.font.a.family,
          this.font.custom1.family,
          this.font.custom2.family,
          this.font.custom3.family,
          this.font.custom4.family,
          this.font.custom5.family,
          this.font.custom6.family
        ]
      }
    });
  }

  next() {
    this.router.navigateByUrl("/button");
  }

  back() {
    this.router.navigateByUrl("/color");
  }
}
