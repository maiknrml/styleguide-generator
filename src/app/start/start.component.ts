import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ObjectsService } from "../services/objects.service";
import { StyleguideService } from "../services/styleguide.service";

@Component({
  selector: "app-start",
  templateUrl: "./start.component.html",
  styleUrls: ["./start.component.scss"]
})
export class StartComponent implements OnInit {
  constructor(
    public sg: StyleguideService,
    public router: Router,
    public objects: ObjectsService
  ) {}

  ngOnInit() {
    let today = new Date().toISOString().substr(0, 10);
    (document.querySelector("#today") as any).value = today;
  }

  next() {
    this.router.navigateByUrl("/color");
  }
}
