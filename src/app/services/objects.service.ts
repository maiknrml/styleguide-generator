import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ObjectsService {
  constructor() {}

  getFont() {
    return {
      h1: {
        size: 40,
        weight: 400,
        italic: false,
        bold: false,
        underline: false,
        spacing: 0,
        family: "Raleway",
        color: "#000"
      },
      h2: {
        size: 32,
        weight: 400,
        italic: false,
        bold: false,
        underline: false,
        spacing: 0,
        family: "Raleway",
        color: "#000"
      },
      h3: {
        size: 28,
        weight: 400,
        italic: false,
        bold: false,
        underline: false,
        spacing: 0,
        family: "Raleway",
        color: "#000"
      },
      h4: {
        size: 24,
        weight: 400,
        italic: false,
        bold: false,
        underline: false,
        spacing: 0,
        family: "Raleway",
        color: "#000"
      },
      h5: {
        size: 20,
        weight: 400,
        italic: false,
        bold: false,
        underline: false,
        spacing: 0,
        family: "Raleway",
        color: "#000"
      },
      h6: {
        size: 16,
        weight: 400,
        italic: false,
        bold: false,
        underline: false,
        spacing: 0,
        family: "Raleway",
        color: "#000"
      },
      p: {
        size: 16,
        weight: 400,
        italic: false,
        bold: false,
        underline: false,
        spacing: 0,
        family: "Raleway",
        color: "#000"
      },
      a: {
        size: 16,
        weight: 400,
        italic: false,
        bold: false,
        underline: true,
        spacing: 0,
        family: "Raleway",
        color: "#000"
      },
      custom1: {
        size: 16,
        weight: 400,
        italic: false,
        bold: false,
        underline: false,
        spacing: 0,
        family: "Raleway",
        color: "#000",
        class: "custom1"
      },
      custom2: {
        size: 16,
        weight: 400,
        italic: false,
        bold: false,
        underline: false,
        spacing: 0,
        family: "Raleway",
        color: "#000",
        class: "custom2"
      },
      custom3: {
        size: 16,
        weight: 400,
        italic: false,
        bold: false,
        underline: false,
        spacing: 0,
        family: "Raleway",
        color: "#000",
        class: "custom3"
      },
      custom4: {
        size: 16,
        weight: 400,
        italic: false,
        bold: false,
        underline: false,
        spacing: 0,
        family: "Raleway",
        color: "#000",
        class: "custom4"
      },
      custom5: {
        size: 16,
        weight: 400,
        italic: false,
        bold: false,
        underline: false,
        spacing: 0,
        family: "Raleway",
        color: "#000",
        class: "custom5"
      },
      custom6: {
        size: 16,
        weight: 400,
        italic: false,
        bold: false,
        underline: false,
        spacing: 0,
        family: "Raleway",
        color: "#000",
        class: "custom6"
      }
    };
  }

  getButton() {
    return [
      {
        size: 16,
        weight: 400,
        family: "Arial",
        color: "red",
        background: "",
        radius: 5,
        marginTop: 10,
        marginRight: 0,
        marginBottom: 0,
        marginLeft: 0,
        paddingTop: 6,
        paddingRight: 12,
        paddingBottom: 6,
        paddingLeft: 12,
        border: "",
        hover: "",
        width: 80,
        height: 40
      },
      {
        class: "custom1",
        family: "Raleway",
        size: 16,
        weight: 400,
        color: "",
        background: "",
        radius: 0.25,
        marginTop: 0,
        marginRight: 0,
        marginBottom: 0,
        marginLeft: 0,
        paddingTop: 6,
        paddingRight: 12,
        paddingBottom: 6,
        paddingLeft: 12,
        border: "",
        hover: "",
        width: 80,
        height: 40
      },
      {
        class: "custom2",
        family: "Raleway",
        size: 16,
        weight: 400,
        color: "",
        background: "",
        radius: 0.25,
        marginTop: 0,
        marginRight: 0,
        marginBottom: 0,
        marginLeft: 0,
        paddingTop: 6,
        paddingRight: 12,
        paddingBottom: 6,
        paddingLeft: 12,
        border: "",
        hover: "",
        width: 80,
        height: 40
      },
      {
        class: "custom3",
        family: "Raleway",
        size: 16,
        weight: 400,
        color: "",
        background: "",
        radius: 0.25,
        marginTop: 0,
        marginRight: 0,
        marginBottom: 0,
        marginLeft: 0,
        paddingTop: 6,
        paddingRight: 12,
        paddingBottom: 6,
        paddingLeft: 12,
        border: "",
        hover: "",
        width: 80,
        height: 40
      },
      {
        class: "custom4",
        family: "Raleway",
        size: 16,
        weight: 400,
        color: "",
        background: "",
        radius: 0.25,
        marginTop: 0,
        marginRight: 0,
        marginBottom: 0,
        marginLeft: 0,
        paddingTop: 6,
        paddingRight: 12,
        paddingBottom: 6,
        paddingLeft: 12,
        border: "#fff",
        hover: "#FEDE3E",
        width: 80,
        height: 40
      },
      {
        class: "custom5",
        family: "Raleway",
        size: 16,
        weight: 400,
        color: "",
        background: "",
        radius: 0.25,
        marginTop: 0,
        marginRight: 0,
        marginBottom: 0,
        marginLeft: 0,
        paddingTop: 6,
        paddingRight: 12,
        paddingBottom: 6,
        paddingLeft: 12,
        border: "",
        hover: "",
        width: 80,
        height: 40
      },
      {
        class: "custom6",
        family: "Raleway",
        size: 16,
        weight: 400,
        color: "",
        background: "",
        radius: 0.25,
        marginTop: 0,
        marginRight: 0,
        marginBottom: 0,
        marginLeft: 0,
        paddingTop: 6,
        paddingRight: 12,
        paddingBottom: 6,
        paddingLeft: 12,
        border: "",
        hover: "",
        width: 80,
        height: 40
      }
    ];
  }

  getColor() {
    return [
      {
        color: "#007bff",
        class: "custom1"
      }
    ];
  }

  getStart() {
    return [
      {
        title: "test",
        date: ""
      }
    ];
  }
}
