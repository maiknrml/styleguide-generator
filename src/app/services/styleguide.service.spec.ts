import { TestBed } from '@angular/core/testing';

import { StyleguideService } from './styleguide.service';

describe('StyleguideService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StyleguideService = TestBed.get(StyleguideService);
    expect(service).toBeTruthy();
  });
});
