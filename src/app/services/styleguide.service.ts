import { Injectable } from '@angular/core';
import { ObjectsService } from './objects.service';

@Injectable({
    providedIn: 'root'
})
export class StyleguideService {

    font = this.objects.getFont();
    button = this.objects.getButton();
    colors = this.objects.getColor();
    infos = this.objects.getStart();

    constructor(public objects: ObjectsService) {
    }


    setFont(font): void {
        // tslint:disable
        for (let element in font) {
            for (let key in font[element]) {
                this.font[element][key] = font[element][key];
            }
        }
    }

    setStart(infos): void {
        for (let element in infos) {
            for (let key in infos[element]) {
                this.infos[element][key] = infos[element][key];
            }
        }
    }


}
