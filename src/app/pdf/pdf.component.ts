import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import * as $ from "jquery";
import * as jsPDF from "jspdf";
import "jspdf-autotable";
import * as Css from "json-to-css";
import { ObjectsService } from "../services/objects.service";
import { StyleguideService } from "../services/styleguide.service";

@Component({
  selector: "app-pdf",
  templateUrl: "./pdf.component.html",
  styleUrls: ["./pdf.component.scss"]
})
export class PdfComponent implements OnInit {
  // button = this.objects.getButton();
  // font = this.objects.getFont();
  // color = this.objects.getColor();
  // infos = this.objects.getStart();

  // // custom colors -> farbpalette
  // color_custom1_color = this.sg.colors.custom1.color;
  // color_custom1_class = this.sg.colors.custom1.class;
  // color_custom2_color = this.sg.colors.custom2.color;
  // color_custom2_class = this.sg.colors.custom2.class;
  // color_custom3_color = this.sg.colors.custom3.color;
  // color_custom3_class = this.sg.colors.custom3.class;
  // color_custom4_color = this.sg.colors.custom4.color;
  // color_custom4_class = this.sg.colors.custom4.class;
  // color_custom5_color = this.sg.colors.custom5.color;
  // color_custom5_class = this.sg.colors.custom5.class;
  // color_custom6_color = this.sg.colors.custom6.color;
  // color_custom6_class = this.sg.colors.custom6.class;

  // color_palette_custom1 = { "background-color": this.sg.colors.custom1.color };
  // color_palette_custom2 = { "background-color": this.sg.colors.custom2.color };
  // color_palette_custom3 = { "background-color": this.sg.colors.custom3.color };
  // color_palette_custom4 = { "background-color": this.sg.colors.custom4.color };
  // color_palette_custom5 = { "background-color": this.sg.colors.custom5.color };
  // color_palette_custom6 = { "background-color": this.sg.colors.custom6.color };

  // // custom fonts
  // font_custom1_class = this.sg.font.custom1.class;
  // font_custom1_family = this.sg.font.custom1.family;
  // font_custom1_bold = this.sg.font.custom1.bold;
  // font_custom1_underline = this.sg.font.custom1.underline;
  // font_custom1_italic = this.sg.font.custom1.italic;
  // font_custom1_size = this.sg.font.custom1.size;
  // font_custom1_weight = this.sg.font.custom1.weight;
  // font_custom1_spacing = this.sg.font.custom1.spacing;
  // font_custom1_color = this.sg.font.custom1.color;
  // font_custom2_class = this.sg.font.custom1.class;
  // font_custom2_family = this.sg.font.custom2.family;
  // font_custom2_bold = this.sg.font.custom2.bold;
  // font_custom2_underline = this.sg.font.custom2.underline;
  // font_custom2_italic = this.sg.font.custom2.italic;
  // font_custom2_size = this.sg.font.custom2.size;
  // font_custom2_weight = this.sg.font.custom2.weight;
  // font_custom2_spacing = this.sg.font.custom2.spacing;
  // font_custom2_color = this.sg.font.custom2.color;
  // font_custom3_class = this.sg.font.custom3.class;
  // font_custom3_family = this.sg.font.custom3.family;
  // font_custom3_bold = this.sg.font.custom3.bold;
  // font_custom3_underline = this.sg.font.custom3.underline;
  // font_custom3_italic = this.sg.font.custom3.italic;
  // font_custom3_size = this.sg.font.custom3.size;
  // font_custom3_weight = this.sg.font.custom3.weight;
  // font_custom3_spacing = this.sg.font.custom3.spacing;
  // font_custom3_color = this.sg.font.custom3.color;
  // font_custom4_class = this.sg.font.custom4.class;
  // font_custom4_family = this.sg.font.custom4.family;
  // font_custom4_bold = this.sg.font.custom4.bold;
  // font_custom4_underline = this.sg.font.custom4.underline;
  // font_custom4_italic = this.sg.font.custom4.italic;
  // font_custom4_size = this.sg.font.custom4.size;
  // font_custom4_weight = this.sg.font.custom4.weight;
  // font_custom4_spacing = this.sg.font.custom4.spacing;
  // font_custom4_color = this.sg.font.custom4.color;
  // font_custom5_class = this.sg.font.custom5.class;
  // font_custom5_family = this.sg.font.custom5.family;
  // font_custom5_bold = this.sg.font.custom5.bold;
  // font_custom5_underline = this.sg.font.custom5.underline;
  // font_custom5_italic = this.sg.font.custom5.italic;
  // font_custom5_size = this.sg.font.custom5.size;
  // font_custom5_weight = this.sg.font.custom5.weight;
  // font_custom5_spacing = this.sg.font.custom5.spacing;
  // font_custom5_color = this.sg.font.custom5.color;
  // font_custom6_class = this.sg.font.custom6.class;
  // font_custom6_family = this.sg.font.custom6.family;
  // font_custom6_bold = this.sg.font.custom6.bold;
  // font_custom6_underline = this.sg.font.custom6.underline;
  // font_custom6_italic = this.sg.font.custom6.italic;
  // font_custom6_size = this.sg.font.custom6.size;
  // font_custom6_weight = this.sg.font.custom6.weight;
  // font_custom6_spacing = this.sg.font.custom6.spacing;
  // font_custom6_color = this.sg.font.custom6.color;

  // // custom buttons
  // button_custom1_class = this.sg.button.custom1.class;
  // button_custom1_family = this.sg.button.custom1.family;
  // button_custom1_size = this.sg.button.custom1.size;
  // button_custom1_weight = this.sg.button.custom1.weight;
  // button_custom1_color = this.sg.button.custom1.color;
  // button_custom1_background = this.sg.button.custom1.background;
  // button_custom1_radius = this.sg.button.custom1.radius;
  // button_custom1_marginTop = this.sg.button.custom1.marginTop;
  // button_custom1_marginRight = this.sg.button.custom1.marginRight;
  // button_custom1_marginBottom = this.sg.button.custom1.marginBottom;
  // button_custom1_marginLeft = this.sg.button.custom1.marginLeft;
  // button_custom1_paddingTop = this.sg.button.custom1.paddingTop;
  // button_custom1_paddingRight = this.sg.button.custom1.paddingRight;
  // button_custom1_paddingBottom = this.sg.button.custom1.paddingBottom;
  // button_custom1_paddingLeft = this.sg.button.custom1.paddingLeft;
  // button_custom1_border = this.sg.button.custom1.border;
  // button_custom1_hover = this.sg.button.custom1.hover;
  // button_custom1_width = this.sg.button.custom1.width;
  // button_custom1_height = this.sg.button.custom1.height;
  // button_custom2_class = this.sg.button.custom2.class;
  // button_custom2_family = this.sg.button.custom2.family;
  // button_custom2_size = this.sg.button.custom2.size;
  // button_custom2_weight = this.sg.button.custom2.weight;
  // button_custom2_color = this.sg.button.custom2.color;
  // button_custom2_background = this.sg.button.custom2.background;
  // button_custom2_radius = this.sg.button.custom2.radius;
  // button_custom2_marginTop = this.sg.button.custom2.marginTop;
  // button_custom2_marginRight = this.sg.button.custom2.marginRight;
  // button_custom2_marginBottom = this.sg.button.custom2.marginBottom;
  // button_custom2_marginLeft = this.sg.button.custom2.marginLeft;
  // button_custom2_paddingTop = this.sg.button.custom2.paddingTop;
  // button_custom2_paddingRight = this.sg.button.custom2.paddingRight;
  // button_custom2_paddingBottom = this.sg.button.custom2.paddingBottom;
  // button_custom2_paddingLeft = this.sg.button.custom2.paddingLeft;
  // button_custom2_border = this.sg.button.custom2.border;
  // button_custom2_hover = this.sg.button.custom2.hover;
  // button_custom2_width = this.sg.button.custom2.width;
  // button_custom2_height = this.sg.button.custom2.height;
  // button_custom3_class = this.sg.button.custom3.class;
  // button_custom3_family = this.sg.button.custom3.family;
  // button_custom3_size = this.sg.button.custom3.size;
  // button_custom3_weight = this.sg.button.custom3.weight;
  // button_custom3_color = this.sg.button.custom3.color;
  // button_custom3_background = this.sg.button.custom3.background;
  // button_custom3_radius = this.sg.button.custom3.radius;
  // button_custom3_marginTop = this.sg.button.custom3.marginTop;
  // button_custom3_marginRight = this.sg.button.custom3.marginRight;
  // button_custom3_marginBottom = this.sg.button.custom3.marginBottom;
  // button_custom3_marginLeft = this.sg.button.custom3.marginLeft;
  // button_custom3_paddingTop = this.sg.button.custom3.paddingTop;
  // button_custom3_paddingRight = this.sg.button.custom3.paddingRight;
  // button_custom3_paddingBottom = this.sg.button.custom3.paddingBottom;
  // button_custom3_paddingLeft = this.sg.button.custom3.paddingLeft;
  // button_custom3_border = this.sg.button.custom3.border;
  // button_custom3_hover = this.sg.button.custom3.hover;
  // button_custom3_width = this.sg.button.custom3.width;
  // button_custom3_height = this.sg.button.custom3.height;
  // button_custom4_class = this.sg.button.custom4.class;
  // button_custom4_family = this.sg.button.custom4.family;
  // button_custom4_size = this.sg.button.custom4.size;
  // button_custom4_weight = this.sg.button.custom4.weight;
  // button_custom4_color = this.sg.button.custom4.color;
  // button_custom4_background = this.sg.button.custom4.background;
  // button_custom4_radius = this.sg.button.custom4.radius;
  // button_custom4_marginTop = this.sg.button.custom4.marginTop;
  // button_custom4_marginRight = this.sg.button.custom4.marginRight;
  // button_custom4_marginBottom = this.sg.button.custom4.marginBottom;
  // button_custom4_marginLeft = this.sg.button.custom4.marginLeft;
  // button_custom4_paddingTop = this.sg.button.custom4.paddingTop;
  // button_custom4_paddingRight = this.sg.button.custom4.paddingRight;
  // button_custom4_paddingBottom = this.sg.button.custom4.paddingBottom;
  // button_custom4_paddingLeft = this.sg.button.custom4.paddingLeft;
  // button_custom4_border = this.sg.button.custom4.border;
  // button_custom4_hover = this.sg.button.custom4.hover;
  // button_custom4_width = this.sg.button.custom4.width;
  // button_custom4_height = this.sg.button.custom4.height;
  // button_custom5_class = this.sg.button.custom5.class;
  // button_custom5_family = this.sg.button.custom5.family;
  // button_custom5_size = this.sg.button.custom5.size;
  // button_custom5_weight = this.sg.button.custom5.weight;
  // button_custom5_color = this.sg.button.custom5.color;
  // button_custom5_background = this.sg.button.custom5.background;
  // button_custom5_radius = this.sg.button.custom5.radius;
  // button_custom5_marginTop = this.sg.button.custom5.marginTop;
  // button_custom5_marginRight = this.sg.button.custom5.marginRight;
  // button_custom5_marginBottom = this.sg.button.custom5.marginBottom;
  // button_custom5_marginLeft = this.sg.button.custom5.marginLeft;
  // button_custom5_paddingTop = this.sg.button.custom5.paddingTop;
  // button_custom5_paddingRight = this.sg.button.custom5.paddingRight;
  // button_custom5_paddingBottom = this.sg.button.custom5.paddingBottom;
  // button_custom5_paddingLeft = this.sg.button.custom5.paddingLeft;
  // button_custom5_border = this.sg.button.custom5.border;
  // button_custom5_hover = this.sg.button.custom5.hover;
  // button_custom5_width = this.sg.button.custom5.width;
  // button_custom5_height = this.sg.button.custom5.height;
  // button_custom6_class = this.sg.button.custom6.class;
  // button_custom6_family = this.sg.button.custom6.family;
  // button_custom6_size = this.sg.button.custom6.size;
  // button_custom6_weight = this.sg.button.custom6.weight;
  // button_custom6_color = this.sg.button.custom6.color;
  // button_custom6_background = this.sg.button.custom6.background;
  // button_custom6_radius = this.sg.button.custom6.radius;
  // button_custom6_marginTop = this.sg.button.custom6.marginTop;
  // button_custom6_marginRight = this.sg.button.custom6.marginRight;
  // button_custom6_marginBottom = this.sg.button.custom6.marginBottom;
  // button_custom6_marginLeft = this.sg.button.custom6.marginLeft;
  // button_custom6_paddingTop = this.sg.button.custom6.paddingTop;
  // button_custom6_paddingRight = this.sg.button.custom6.paddingRight;
  // button_custom6_paddingBottom = this.sg.button.custom6.paddingBottom;
  // button_custom6_paddingLeft = this.sg.button.custom6.paddingLeft;
  // button_custom6_border = this.sg.button.custom6.border;
  // button_custom6_hover = this.sg.button.custom6.hover;
  // button_custom6_width = this.sg.button.custom6.width;
  // button_custom6_height = this.sg.button.custom6.height;

  constructor(
    public sg: StyleguideService,
    public router: Router,
    public objects: ObjectsService
  ) {}

  // downloadPDF() {
  //   const doc = new jsPDF("p", "pt", "a4");

  //   const specialElementHandlers = {
  //     "#editor"(element, renderer) {
  //       return true;
  //     }
  //   };

  //   doc.addHTML(document.getElementById("content"), function() {
  //     doc.save("Styleguide.pdf");
  //   });
  // }

  downloadJSON() {
    // const json = Object.assign(this.color, this.font, this.button);
    const data =
      "text/json;charset=utf-8," +
      encodeURIComponent(JSON.stringify(this.sg.colors, 0, 4));
    $(
      '<a href="data:' +
        data +
        '" download="styleguide.json" style="color:#495057;text-decoration:none;">Download JSON <i class="fa fa-file-code" style="font-size:30px;color:#495057;"></i></a>'
    ).appendTo("#jsonButton");
  }

  // downloadCSS() {
  //   const json = Object.assign(this.button, this.color, this.font);
  //   const css = Css.of(json);
  //   const data = "text/css;charset=utf-8," + encodeURIComponent(css);
  //   $(
  //     '<a href="data:' +
  //       data +
  //       '" download="data"style="color:#495057;text-decoration:none;">Download CSS <i class="fab fa-css3-alt" style="font-size:30px;color:#495057;"></i></a>'
  //   ).appendTo("#cssButton");
  // }

  ngOnInit() {
    this.downloadJSON();
    // this.downloadCSS();
  }

  back() {
    this.router.navigateByUrl("/button");
  }
}
