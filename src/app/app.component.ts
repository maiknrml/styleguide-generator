import { Component } from "@angular/core";
import * as WebFont from "webfontloader";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "styleguide";

  constructor() {
    WebFont.load({
      google: {
        families: ["Raleway"]
      }
    });
  }
}
