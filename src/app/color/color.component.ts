import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { StyleguideService } from "../services/styleguide.service";
import { ObjectsService } from "../services/objects.service";

@Component({
  selector: "app-color",
  templateUrl: "./color.component.html",
  styleUrls: ["./color.component.scss"]
})
export class ColorComponent implements OnInit {
  constructor(public sg: StyleguideService, public router: Router) {}

  ngOnInit() {}

  next() {
    this.router.navigateByUrl("/font");
  }

  back() {
    this.router.navigateByUrl("/start");
  }

  pushToArray(i) {
    i = i + 1;
    this.sg.colors.push(this.sg.colors[i]);
    console.log(i + 1);
  }
}
