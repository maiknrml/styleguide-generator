import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ObjectsService } from "../services/objects.service";
import { StyleguideService } from "../services/styleguide.service";

@Component({
  selector: "app-testing",
  templateUrl: "./testing.component.html",
  styleUrls: ["./testing.component.scss"]
})
export class TestingComponent implements OnInit {
  showCustomButtons = [];
  constructor(
    public sg: StyleguideService,
    public router: Router,
    public objects: ObjectsService
  ) {
    this.applyChanges();
    this.setHover();
    this.initShowCustomButtons();
  }

  ngOnInit() {
    console.log("Farben:\n");
    console.log(this.sg.colors[0].color);
  }

  applyChanges() {
    $(document).ready(() => {
      setTimeout(() => {
        // size
        $("button.change").css("font-size", this.sg.button[0].size + "px");
        // weight
        $("button.change").css("font-weight", this.sg.button[0].weight + "");
        // family
        $("button.change").css("font-family", this.sg.button[0].family + "");
        // width
        $("button.change").css("width", this.sg.button[0].width + "px");
        // height
        $("button.change").css("height", this.sg.button[0].height + "px");
        // color
        // $("button.change").css("color", this.sg.colors[0].color);
        // background - color;
        $("button.change").css(
          "background-color",
          this.sg.button[0].background + ""
        );
        // radius
        $("button.change").css("border-radius", this.sg.button[0].radius + "%");
        // margin
        $("button.change").css(
          "margin-top",
          this.sg.button[0].marginTop + "px"
        );
        $("button.change").css(
          "margin-right",
          this.sg.button[0].marginRight + "px"
        );
        $("button.change").css(
          "margin-bottom",
          this.sg.button[0].marginBottom + "px"
        );
        $("button.change").css(
          "margin-left",
          this.sg.button[0].marginLeft + "px"
        );
        // padding
        $("button.change").css(
          "padding-top",
          this.sg.button[0].paddingTop + "px"
        );
        $("button.change").css(
          "padding-right",
          this.sg.button[0].paddingRight + "px"
        );
        $("button.change").css(
          "padding-bottom",
          this.sg.button[0].paddingBottom + "px"
        );
        $("button.change").css(
          "padding-left",
          this.sg.button[0].paddingLeft + "px"
        );
        // border-color
        $("button.change").css("border-color", this.sg.button[0].border);
      }, 100);
    });
  }

  setHover() {
    // hover
    // const hover = this.sg.button[0].hover;
    // const bg = this.sg.button[0].background;
    // console.warn(hover);
    // $(document).ready(() => {
    //   $(".change").hover(
    //     function() {
    //       $(this).css("background-color", hover);
    //     },
    //     function() {
    //       $(this).css("background-color", bg);
    //     }
    //   );
    // });
  }

  next() {
    this.router.navigateByUrl("/pdf");
  }

  back() {
    this.router.navigateByUrl("/font");
  }

  initShowCustomButtons() {
    for (let i = 0; i < 6; i++) {
      this.showCustomButtons.push(false);
    }
  }

  toggleShowButton(i: number) {
    this.showCustomButtons.fill(false);
    this.showCustomButtons[i] = true;
  }
}
