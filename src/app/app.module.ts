import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule, Routes } from "@angular/router";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ColorPickerModule } from "ngx-color-picker";

import { AppComponent } from "./app.component";
import { HeaderComponent } from "./header/header.component";
import { FontComponent } from "./font/font.component";
import { ButtonComponent } from "./button/button.component";
import { PdfComponent } from "./pdf/pdf.component";
import { ColorPickerComponent } from "./color-picker/color-picker.component";
import { ColorComponent } from "./color/color.component";
import { AngularFontAwesomeModule } from "angular-font-awesome";
import { StartComponent } from "./start/start.component";
import { TestingComponent } from "./testing/testing.component";

const appRoutes: Routes = [
  { path: "", redirectTo: "/start", pathMatch: "full" },
  { path: "start", component: StartComponent },
  { path: "color", component: ColorComponent },
  { path: "font", component: FontComponent },
  { path: "button", component: ButtonComponent },
  { path: "pdf", component: PdfComponent },
  { path: "testing", component: TestingComponent },
  { path: "**", redirectTo: "/start", pathMatch: "full" }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FontComponent,
    ButtonComponent,
    PdfComponent,
    ColorPickerComponent,
    ColorComponent,
    StartComponent,
    TestingComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot(appRoutes, { enableTracing: false }),
    NgbModule,
    ColorPickerModule,
    AngularFontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
